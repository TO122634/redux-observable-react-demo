import { createEpicMiddleware } from 'redux-observable'
import { applyMiddleware } from 'redux'
import pingEpic from './pingEpic'

const epicMiddleware = createEpicMiddleware(pingEpic)
export default applyMiddleware(epicMiddleware)