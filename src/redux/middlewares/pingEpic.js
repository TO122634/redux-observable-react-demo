import 'rxjs/add/operator/delay'
import 'rxjs/add/operator/mapTo'

const PING = 'PING';
const PONG = 'PONG';

const pingEpic = action$ =>
    action$.ofType(PING)
        .delay(1000) // Asynchronously wait 1000ms then continue
        .mapTo({ type: PONG });

export default pingEpic