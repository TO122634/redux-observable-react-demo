import { combineReducers } from 'redux';
import pingpongReducer from './pingpong'

export default combineReducers({
    pingpong: pingpongReducer
});