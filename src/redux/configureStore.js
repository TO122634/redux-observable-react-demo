import { createStore, compose } from 'redux'
import reducer from './modules/reducer'
import middleware from './middlewares/middleware'

// Required for setting up a store using middlewares and enabling redux-dev-tools
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
    reducer,
    /* preloadedState, */
    composeEnhancers(middleware)
)

export default store