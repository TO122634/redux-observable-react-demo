import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from 'material-ui/styles';
import Button from 'material-ui/Button';
import AddIcon from 'material-ui-icons/Add';
import { connect } from 'react-redux';
import flowRight from 'lodash/flowRight'
import { pingAction } from '../redux/modules/pingpong'

const styles = {}

const Pingpong = ({ isPinging, ping, classes }) => (
  <div>
    <Button
      fab
      color={isPinging ? "accent" : "primary"}
      onClick={ping}
      aria-label="edit"
      className={classes.button}>
      <AddIcon />
    </Button>
  </div>
);

Pingpong.propTypes = {
  isPinging: PropTypes.bool.isRequired,
  ping: PropTypes.func.isRequired,
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = state => ({
  isPinging: state.pingpong.isPinging,
})

const mapDispatchToProps = dispatch => ({
  ping: () => dispatch(pingAction())
})

export default flowRight(
  withStyles(styles),
  connect(mapStateToProps, mapDispatchToProps)
)(Pingpong);